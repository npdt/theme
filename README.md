# Tailwind theme

Tailwind theme used across npdt's projects.

## Install

```
npm i @npdtdev/theme
```

## Usage

In your stylesheet document import in your css file.

```
@import '@npdtdev/theme/themes/base.css';
```

and add the theme to your body such as:

```
<body data-theme="base">
```
